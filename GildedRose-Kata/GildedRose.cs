using System;
using System.Collections.Generic;
using GildedRose_Kata.Extensions;
using GildedRose_Kata.Models;
using MoreLinq;

namespace GildedRose_Kata
{
    public class GildedRose
    {
        IList<Item> Items;
        public GildedRose(IList<Item> Items)
        {
            this.Items = Items;
        }

        public void UpdateQuality()
        {
            Items.ForEach(item => UpdateItem(item));
        }

        private void UpdateItem(Item item)
        {
            if (item.Name.Contains("Conjured", StringComparison.OrdinalIgnoreCase))
            {
                item.UpdateConjuredItem();
                return;
            }
            
            switch (item.Name)
            {
                case "Sulfuras, Hand of Ragnaros":
                    item.UpdateLegendaryItem();
                    break;
                case "Aged Brie":
                    item.UpdateAgedItem();
                    break;
                case "Backstage passes to a TAFKAL80ETC concert":
                    item.UpdatePassItem();
                    break;
                default:
                    item.UpdateNormalItem();
                    break;
            }
        }
    }
}