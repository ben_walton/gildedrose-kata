using System;
using GildedRose_Kata.Models;

namespace GildedRose_Kata.Extensions
{
    public static class ItemExtensions
    {
        private const int LegendaryQuality = 80;
        private const int MinItemQuality = 0;
        private const int MaxItemQuality = 50;
        
        public static void UpdateAgedItem(this Item item)
        {
            UpdateSellIn(item);

            if (item.SellIn < 0) item.Quality += 1;
            item.Quality += 1;

            LimitQualityRange(item);
        }

        public static void UpdateConjuredItem(this Item item)
        {
            UpdateSellIn(item);
            
            if (item.SellIn < 0) item.Quality -= 2;
            item.Quality -= 2;

            LimitQualityRange(item);
        }
        
        public static void UpdateLegendaryItem(this Item item)
        {
            item.Quality = LegendaryQuality;
        }

        public static void UpdateNormalItem(this Item item)
        {
            UpdateSellIn(item);
            
            if (item.SellIn < 0) item.Quality -= 1;
            item.Quality -= 1;

            LimitQualityRange(item);
        }

        public static void UpdatePassItem(this Item item)
        {
            UpdateSellIn(item);
            
            item.Quality += 1;
            if (item.SellIn < 10) item.Quality += 1;
            if (item.SellIn < 5) item.Quality += 1;
            if (item.SellIn < 0) item.Quality = 0;

            LimitQualityRange(item);
        }

        private static void LimitQualityRange(Item item)
        {
            item.Quality = Math.Max(Math.Min(item.Quality, MaxItemQuality), MinItemQuality);
        }

        private static void UpdateSellIn(Item item)
        {
            item.SellIn -= 1;
        }
    }
}