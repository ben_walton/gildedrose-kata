using System.Collections.Generic;
using GildedRose_Kata.Models;
using NUnit.Framework;

namespace GildedRose_Kata.Tests.Integration
{
    public class GildedRoseTests
    {
        private GildedRose sut;

        [Test]
        public void GivenNormalItemWithPositiveSellIn_WhenUpdateQuality_ThenSellInOneLess()
        {
            var originalSellIn = 10;
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = originalSellIn,
                Quality = 10
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(originalSellIn - 1, normalItem.SellIn);
        }

        [Test]
        public void GivenNormalItemWithZeroSellIn_WhenUpdateQuality_ThenSellInOneLess()
        {
            var originalSellIn = 0;
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = originalSellIn,
                Quality = 10
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(originalSellIn - 1, normalItem.SellIn);
        }

        [Test]
        public void GivenNormalItemWithNegativeSellIn_WhenUpdateQuality_ThenSellInOneLess()
        {
            var originalSellIn = -10;
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = originalSellIn,
                Quality = 10
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(originalSellIn - 1, normalItem.SellIn);
        }

        [Test]
        public void GivenNormalItemWithPositiveQualityAndPositiveSellIn_WhenUpdateQuality_ThenQualityIsOneLess()
        {
            var originalQuality = 10;
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = 10,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality - 1, normalItem.Quality);
        }

        [Test]
        public void GivenNormalItemWithZeroQualityAndPositiveSellIn_WhenUpdateQuality_ThenQualityIsZero()
        {
            var originalQuality = 0;
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = 10,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality, normalItem.Quality);
        }

        [Test]
        public void GivenNormalItemWithPositiveQualityAndZeroSellIn_WhenUpdateQuality_ThenQualityIsTwoLess()
        {
            var originalQuality = 10;
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = 0,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality - 2, normalItem.Quality);
        }

        [Test]
        public void GivenNormalItemWithTwoQualityAndZeroSellIn_WhenUpdateQuality_ThenQualityIsZero()
        {
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = 0,
                Quality = 2
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(0, normalItem.Quality);
        }

        [Test]
        public void GivenNormalItemWithOneQualityAndZeroSellIn_WhenUpdateQuality_ThenQualityIsZero()
        {
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = 0,
                Quality = 1
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(0, normalItem.Quality);
        }

        [Test]
        public void GivenNormalItemWithZeroQualityAndZeroSellIn_WhenUpdateQuality_ThenQualityIsZero()
        {
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = 0,
                Quality = 0
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(0, normalItem.Quality);
        }

        [Test]
        public void GivenNormalItemWithPositiveQualityAndNegativeSellIn_WhenUpdateQuality_ThenQualityIsTwoLess()
        {
            var originalQuality = 10;
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = -10,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality - 2, normalItem.Quality);
        }

        [Test]
        public void GivenNormalItemWithTwoQualityAndNegativeSellIn_WhenUpdateQuality_ThenQualityIsZero()
        {
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = -10,
                Quality = 2
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(0, normalItem.Quality);
        }

        [Test]
        public void GivenNormalItemWithOneQualityAndNegativeSellIn_WhenUpdateQuality_ThenQualityIsZero()
        {
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = -10,
                Quality = 1
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(0, normalItem.Quality);
        }

        [Test]
        public void GivenNormalItemWithZeroQualityAndNegativeSellIn_WhenUpdateQuality_ThenQualityIsZero()
        {
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = -10,
                Quality = 0
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(0, normalItem.Quality);
        }

        [Test]
        public void GivenNormalItemWithFiftyOneQualityAndPositiveSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var normalItem = new Item
            {
                Name = "Normal Item",
                SellIn = 10,
                Quality = 51
            };
            sut = new GildedRose(new List<Item> {normalItem});

            sut.UpdateQuality();

            Assert.AreEqual(50, normalItem.Quality);
        }

        [Test]
        public void GivenAgedBrieWithPositiveSellIn_WhenUpdateQuality_ThenSellInOneLess()
        {
            var originalSellIn = 10;
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = originalSellIn,
                Quality = 10
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(originalSellIn - 1, agedBrie.SellIn);
        }

        [Test]
        public void GivenAgedBrieWithZeroSellIn_WhenUpdateQuality_ThenSellInOneLess()
        {
            var originalSellIn = 0;
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = originalSellIn,
                Quality = 10
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(originalSellIn - 1, agedBrie.SellIn);
        }

        [Test]
        public void GivenAgedBrieWithNegativeSellIn_WhenUpdateQuality_ThenSellInOneLess()
        {
            var originalSellIn = -10;
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = originalSellIn,
                Quality = 10
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(originalSellIn - 1, agedBrie.SellIn);
        }

        [Test]
        public void GivenAgedBrieWithPositiveQualityAndPositiveSellIn_WhenUpdateQuality_ThenQualityIsOneMore()
        {
            var originalQuality = 10;
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = 10,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 1, agedBrie.Quality);
        }

        [Test]
        public void GivenAgedBrieWithFortyNineQualityAndPositiveSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = 10,
                Quality = 49
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(50, agedBrie.Quality);
        }

        [Test]
        public void GivenAgedBrieWithFiftyQualityAndPositiveSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = 10,
                Quality = 50
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(50, agedBrie.Quality);
        }

        [Test]
        public void GivenAgedBrieWithPositiveQualityAndZeroSellIn_WhenUpdateQuality_ThenQualityIsTwoMore()
        {
            var originalQuality = 10;
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = 0,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 2, agedBrie.Quality);
        }

        [Test]
        public void GivenAgedBrieWithFortyEightQualityAndZeroSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = 0,
                Quality = 48
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(50, agedBrie.Quality);
        }

        [Test]
        public void GivenAgedBrieWithFortyNineQualityAndZeroSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = 0,
                Quality = 49
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(50, agedBrie.Quality);
        }

        [Test]
        public void GivenAgedBrieWithFiftyQualityAndZeroSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = 0,
                Quality = 50
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(50, agedBrie.Quality);
        }

        [Test]
        public void GivenAgedBrieWithPositiveQualityAndNegativeSellIn_WhenUpdateQuality_ThenQualityIsTwoMore()
        {
            var originalQuality = 10;
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = -10,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 2, agedBrie.Quality);
        }

        [Test]
        public void GivenAgedBrieWithFortyEightQualityAndNegativeSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = -10,
                Quality = 48
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(50, agedBrie.Quality);
        }

        [Test]
        public void GivenAgedBrieWithFortyNineQualityAndNegativeSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = -10,
                Quality = 49
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(50, agedBrie.Quality);
        }

        [Test]
        public void GivenAgedBrieWithFiftyQualityAndNegativeSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = -10,
                Quality = 50
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(50, agedBrie.Quality);
        }

        [Test]
        public void GivenAgedBrieWithZeroQualityAndPositiveSellIn_WhenUpdateQuality_ThenQualityIsOne()
        {
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = 10,
                Quality = 0
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(1, agedBrie.Quality);
        }

        [Test]
        public void GivenAgedBrieWithZeroQualityAndZeroSellIn_WhenUpdateQuality_ThenQualityIsTwo()
        {
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = 0,
                Quality = 0
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(2, agedBrie.Quality);
        }

        [Test]
        public void GivenAgedBrieWithZeroQualityAndNegativeSellIn_WhenUpdateQuality_ThenQualityIsTwo()
        {
            var agedBrie = new Item
            {
                Name = "Aged Brie",
                SellIn = -10,
                Quality = 0
            };
            sut = new GildedRose(new List<Item> {agedBrie});

            sut.UpdateQuality();

            Assert.AreEqual(2, agedBrie.Quality);
        }

        [Test]
        public void GivenSulfurasWithPositiveSellIn_WhenUpdateQuality_ThenSellInUnchanged()
        {
            var sulfuras = new Item
            {
                Name = "Sulfuras, Hand of Ragnaros",
                SellIn = 10,
                Quality = 80
            };
            sut = new GildedRose(new List<Item> {sulfuras});

            sut.UpdateQuality();

            Assert.AreEqual(10, sulfuras.SellIn);
        }

        [Test]
        public void GivenSulfurasWithZeroSellIn_WhenUpdateQuality_ThenSellInUnchanged()
        {
            var sulfuras = new Item
            {
                Name = "Sulfuras, Hand of Ragnaros",
                SellIn = 0,
                Quality = 80
            };
            sut = new GildedRose(new List<Item> {sulfuras});

            sut.UpdateQuality();

            Assert.AreEqual(0, sulfuras.SellIn);
        }

        [Test]
        public void GivenSulfurasWithNegativeSellIn_WhenUpdateQuality_ThenSellInUnchanged()
        {
            var sulfuras = new Item
            {
                Name = "Sulfuras, Hand of Ragnaros",
                SellIn = -10,
                Quality = 80
            };
            sut = new GildedRose(new List<Item> {sulfuras});

            sut.UpdateQuality();

            Assert.AreEqual(-10, sulfuras.SellIn);
        }

        [Test]
        public void GivenSulfurasWithPositiveSellIn_WhenUpdateQuality_ThenQualityUnchanged()
        {
            var sulfuras = new Item
            {
                Name = "Sulfuras, Hand of Ragnaros",
                SellIn = 10,
                Quality = 80
            };
            sut = new GildedRose(new List<Item> {sulfuras});

            sut.UpdateQuality();

            Assert.AreEqual(80, sulfuras.Quality);
        }

        [Test]
        public void GivenSulfurasWithZeroSellIn_WhenUpdateQuality_ThenQualityUnchanged()
        {
            var sulfuras = new Item
            {
                Name = "Sulfuras, Hand of Ragnaros",
                SellIn = 0,
                Quality = 80
            };
            sut = new GildedRose(new List<Item> {sulfuras});

            sut.UpdateQuality();

            Assert.AreEqual(80, sulfuras.Quality);
        }

        [Test]
        public void GivenSulfurasWithNegativeSellIn_WhenUpdateQuality_ThenQualityUnchanged()
        {
            var sulfuras = new Item
            {
                Name = "Sulfuras, Hand of Ragnaros",
                SellIn = -10,
                Quality = 80
            };
            sut = new GildedRose(new List<Item> {sulfuras});

            sut.UpdateQuality();

            Assert.AreEqual(80, sulfuras.Quality);
        }

        [Test]
        public void GivenBackstagePassWithPositiveSellIn_WhenUpdateQuality_ThenSellInOneLess()
        {
            var originalSellIn = 10;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = originalSellIn,
                Quality = 10
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalSellIn - 1, backstagePass.SellIn);
        }

        [Test]
        public void GivenBackstagePassWithZeroSellIn_WhenUpdateQuality_ThenSellInOneLess()
        {
            var originalSellIn = 0;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = originalSellIn,
                Quality = 10
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalSellIn - 1, backstagePass.SellIn);
        }

        [Test]
        public void GivenBackstagePassWithNegativeSellIn_WhenUpdateQuality_ThenSellInOneLess()
        {
            var originalSellIn = -10;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = originalSellIn,
                Quality = 10
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalSellIn - 1, backstagePass.SellIn);
        }

        [Test]
        public void GivenBackstagePassWithElevenSellIn_WhenUpdateQuality_ThenQualityOneMore()
        {
            var originalQuality = 10;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 11,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 1, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithTenSellIn_WhenUpdateQuality_ThenQualityTwoMore()
        {
            var originalQuality = 10;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 10,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 2, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithNineSellIn_WhenUpdateQuality_ThenQualityTwoMore()
        {
            var originalQuality = 10;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 9,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 2, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithSixSellIn_WhenUpdateQuality_ThenQualityTwoMore()
        {
            var originalQuality = 10;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 6,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 2, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFiveSellIn_WhenUpdateQuality_ThenQualityThreeMore()
        {
            var originalQuality = 10;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 5,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 3, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFourSellIn_WhenUpdateQuality_ThenQualityThreeMore()
        {
            var originalQuality = 10;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 4,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 3, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithOneSellIn_WhenUpdateQuality_ThenQualityThreeMore()
        {
            var originalQuality = 10;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 1,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 3, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithZeroSellIn_WhenUpdateQuality_ThenQualityZero()
        {
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 0,
                Quality = 10
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(0, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithNegativeOneSellIn_WhenUpdateQuality_ThenQualityZero()
        {
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = -1,
                Quality = 10
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(0, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFortyEightQualityAndElevenSellIn_WhenUpdateQuality_ThenQualityIsFortyNine()
        {
            var originalQuality = 48;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 11,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 1, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFortyNineQualityAndElevenSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var originalQuality = 49;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 11,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 1, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFiftyQualityAndElevenSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var originalQuality = 50;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 11,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFortySevenQualityAndSixSellIn_WhenUpdateQuality_ThenQualityIsFortyNine()
        {
            var originalQuality = 47;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 6,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 2, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFortyEightQualityAndSixSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var originalQuality = 48;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 6,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 2, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFortyNineQualityAndSixSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var originalQuality = 49;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 6,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 1, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFiftyQualityAndSixSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var originalQuality = 50;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 6,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFortySixQualityAndOneSellIn_WhenUpdateQuality_ThenQualityIsFortyNine()
        {
            var originalQuality = 46;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 1,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 3, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFortySevenQualityAndOneSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var originalQuality = 47;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 1,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 3, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFortyEightQualityAndOneSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var originalQuality = 48;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 1,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 2, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFortyNineQualityAndOneSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var originalQuality = 49;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 1,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality + 1, backstagePass.Quality);
        }

        [Test]
        public void GivenBackstagePassWithFiftyQualityAndOneSellIn_WhenUpdateQuality_ThenQualityIsFifty()
        {
            var originalQuality = 50;
            var backstagePass = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 1,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {backstagePass});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality, backstagePass.Quality);
        }

        [Test]
        public void GivenConjuredItemWithPositiveSellIn_WhenUpdateQuality_ThenSellInOneLess()
        {
            var originalSellIn = 10;
            var conjuredItem = new Item
            {
                Name = "Conjured Item",
                SellIn = originalSellIn,
                Quality = 10
            };
            sut = new GildedRose(new List<Item> {conjuredItem});

            sut.UpdateQuality();

            Assert.AreEqual(originalSellIn - 1, conjuredItem.SellIn);
        }

        [Test]
        public void GivenConjuredItemWithQualityTenAndPositiveSellIn_WhenUpdateQuality_ThenQualityIsEight()
        {
            var originalQuality = 10;
            var conjuredItem = new Item
            {
                Name = "Conjured Item",
                SellIn = 10,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {conjuredItem});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality - 2, conjuredItem.Quality);
        }

        [Test]
        public void GivenConjuredItemWithQualityTenAndZeroSellIn_WhenUpdateQuality_ThenQualityIsSix()
        {
            var originalQuality = 10;
            var conjuredItem = new Item
            {
                Name = "Conjured Item",
                SellIn = 0,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {conjuredItem});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality - 4, conjuredItem.Quality);
        }

        [Test]
        public void GivenLowerCaseConjuredItemWithQualityTenAndPositiveSellIn_WhenUpdateQuality_ThenQualityIsEight()
        {
            var originalQuality = 10;
            var conjuredItem = new Item
            {
                Name = "conjured Item",
                SellIn = 10,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {conjuredItem});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality - 2, conjuredItem.Quality);
        }

        [Test]
        public void GivenLowerCaseConjuredItemWithQualityTenAndZeroSellIn_WhenUpdateQuality_ThenQualityIsSix()
        {
            var originalQuality = 10;
            var conjuredItem = new Item
            {
                Name = "conjured Item",
                SellIn = 0,
                Quality = originalQuality
            };
            sut = new GildedRose(new List<Item> {conjuredItem});

            sut.UpdateQuality();

            Assert.AreEqual(originalQuality - 4, conjuredItem.Quality);
        }
    }
}