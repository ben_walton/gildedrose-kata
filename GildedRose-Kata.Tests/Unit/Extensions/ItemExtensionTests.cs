using GildedRose_Kata.Extensions;
using GildedRose_Kata.Models;
using NUnit.Framework;

namespace GildedRose_Kata.Tests.Unit.Extensions
{
    public class ItemExtensionTests
    {
        private Item sut;
        
        [Test]
        public void GivenQualityNot80_WhenUpdateLegendaryItem_ThenQuantityIs80()
        {
            sut = new Item
            {
                Name = "Legendary",
                SellIn = 20,
                Quality = 20
            };

            sut.UpdateLegendaryItem();
            
            Assert.AreEqual(80, sut.Quality);
        }

        [Test]
        public void GivenPositiveSellIn_WhenUpdateNormalItem_ThenSellInDecreasedByOne()
        {
            var originalSellIn = 20;
            sut = new Item
            {
                Name = "Normal",
                SellIn = originalSellIn,
                Quality = 20
            };
            
            sut.UpdateNormalItem();
            
            Assert.AreEqual(originalSellIn - 1, sut.SellIn);
        }

        [Test]
        public void GivenQuality15AndPositiveSellIn_WhenUpdateNormalItem_ThenQualityIs14()
        {
            var originalQuality = 15;
            sut = new Item
            {
                Name = "Normal",
                SellIn = 20,
                Quality = originalQuality
            };
            
            sut.UpdateNormalItem();
            
            Assert.AreEqual(originalQuality - 1, sut.Quality);
        }

        [Test]
        public void GivenQuality15AndZeroSellIn_WhenUpdateNormalItem_ThenQualityIs13()
        {
            var originalQuality = 15;
            sut = new Item
            {
                Name = "Normal",
                SellIn = 0,
                Quality = originalQuality
            };
            
            sut.UpdateNormalItem();
            
            Assert.AreEqual(originalQuality - 2, sut.Quality);
        }

        [Test]
        public void GivenQuality15AndNegativeSellIn_WhenUpdateNormalItem_ThenQualityIs13()
        {
            var originalQuality = 15;
            sut = new Item
            {
                Name = "Normal",
                SellIn = -10,
                Quality = originalQuality
            };
            
            sut.UpdateNormalItem();
            
            Assert.AreEqual(originalQuality - 2, sut.Quality);
        }

        [Test]
        public void GivenQuality55_WhenUpdateNormalItem_ThenQualityIs50()
        {
            sut = new Item
            {
                Name = "Normal",
                SellIn = 20,
                Quality = 55
            };
            
            sut.UpdateNormalItem();
            
            Assert.AreEqual(50, sut.Quality);
        }

        [Test]
        public void GivenQualityZero_WhenUpdateNormalItem_ThenQualityIsZero()
        {
            var originalQuality = 0;
            sut = new Item
            {
                Name = "Normal",
                SellIn = 20,
                Quality = originalQuality
            };
            
            sut.UpdateNormalItem();
            
            Assert.AreEqual(originalQuality, sut.Quality);
        }

        [Test]
        public void GivenPositiveSellIn_WhenUpdateAgedItem_ThenSellInDecreasesByOne()
        {
            var originalSellIn = 10;
            sut = new Item
            {
                Name = "Aged",
                SellIn = originalSellIn,
                Quality = 10
            };

            sut.UpdateAgedItem();
            
            Assert.AreEqual(originalSellIn - 1, sut.SellIn);
        }

        [Test]
        public void GivenQuality15AndPositiveSellIn_WhenUpdateAgedItem_ThenQualityIs16()
        {
            var originalQuality = 15;
            sut = new Item
            {
                Name = "Aged",
                SellIn = 10,
                Quality = originalQuality
            };

            sut.UpdateAgedItem();
            
            Assert.AreEqual(originalQuality + 1, sut.Quality);
        }

        [Test]
        public void GivenQuality15AndZeroSellIn_WhenUpdateAgedItem_ThenQualityIs17()
        {
            var originalQuality = 15;
            sut = new Item
            {
                Name = "Aged",
                SellIn = 0,
                Quality = originalQuality
            };
            
            sut.UpdateAgedItem();
            
            Assert.AreEqual(originalQuality + 2, sut.Quality);
        }

        [Test]
        public void GivenQuality15AndNegativeSellIn_WhenUpdateAgedItem_ThenQualityIs17()
        {
            var originalQuality = 15;
            sut = new Item
            {
                Name = "Aged",
                SellIn = -10,
                Quality = originalQuality
            };
            
            sut.UpdateAgedItem();
            
            Assert.AreEqual(originalQuality + 2, sut.Quality);
        }

        [Test]
        public void GivenQualityNegative5_WhenUpdateAgedItemItem_ThenQualityIsZero()
        {
            sut = new Item
            {
                Name = "Aged",
                SellIn = 20,
                Quality = -5
            };
            
            sut.UpdateAgedItem();
            
            Assert.AreEqual(0, sut.Quality);
        }

        [Test]
        public void GivenQuality50_WhenUpdateAgedItemItem_ThenQualityIs50()
        {
            sut = new Item
            {
                Name = "Aged",
                SellIn = 20,
                Quality = 50
            };
            
            sut.UpdateAgedItem();
            
            Assert.AreEqual(50, sut.Quality);
        }

        [Test]
        public void GivenPositiveSellIn_WhenUpdatePassItem_ThenSellInDecreasedByOne()
        {
            var originalSellIn = 10;
            sut = new Item
            {
                Name = "Pass",
                SellIn = originalSellIn,
                Quality = 10
            };

            sut.UpdatePassItem();
            
            Assert.AreEqual(originalSellIn - 1, sut.SellIn);
        }

        [Test]
        public void GivenElevenSellIn_WhenUpdatePassItem_ThenQualityIncreasesByOne()
        {
            var originalQuality = 10;
            sut = new Item
            {
                Name = "Pass",
                SellIn = 11,
                Quality = originalQuality
            };

            sut.UpdatePassItem();
            
            Assert.AreEqual(originalQuality + 1, sut.Quality);
        }

        [Test]
        public void GivenTenSellIn_WhenUpdatePassItem_ThenQualityIncreasesByTwo()
        {
            var originalQuality = 10;
            sut = new Item
            {
                Name = "Pass",
                SellIn = 10,
                Quality = originalQuality
            };

            sut.UpdatePassItem();
            
            Assert.AreEqual(originalQuality + 2, sut.Quality);
        }

        [Test]
        public void GivenNineSellIn_WhenUpdatePassItem_ThenQualityIncreasesByTwo()
        {
            var originalQuality = 10;
            sut = new Item
            {
                Name = "Pass",
                SellIn = 9,
                Quality = originalQuality
            };

            sut.UpdatePassItem();
            
            Assert.AreEqual(originalQuality + 2, sut.Quality);
        }

        [Test]
        public void GivenFiveSellIn_WhenUpdatePassItem_ThenQualityIncreasesByThree()
        {
            var originalQuality = 10;
            sut = new Item
            {
                Name = "Pass",
                SellIn = 5,
                Quality = originalQuality
            };

            sut.UpdatePassItem();
            
            Assert.AreEqual(originalQuality + 3, sut.Quality);
        }

        [Test]
        public void GivenFourSellIn_WhenUpdatePassItem_ThenQualityIncreasesByThree()
        {
            var originalQuality = 10;
            sut = new Item
            {
                Name = "Pass",
                SellIn = 4,
                Quality = originalQuality
            };

            sut.UpdatePassItem();
            
            Assert.AreEqual(originalQuality + 3, sut.Quality);
        }

        [Test]
        public void GivenOneSellIn_WhenUpdatePassItem_ThenQualityIncreasesByThree()
        {
            var originalQuality = 10;
            sut = new Item
            {
                Name = "Pass",
                SellIn = 1,
                Quality = originalQuality
            };

            sut.UpdatePassItem();
            
            Assert.AreEqual(originalQuality + 3, sut.Quality);
        }

        [Test]
        public void GivenZeroSellIn_WhenUpdatePassItem_ThenQualityIsZero()
        {
            sut = new Item
            {
                Name = "Pass",
                SellIn = 0,
                Quality = 10
            };

            sut.UpdatePassItem();
            
            Assert.AreEqual(0, sut.Quality);
        }

        [Test]
        public void GivenNegativeSellIn_WhenUpdatePassItem_ThenQualityIsZero()
        {
            sut = new Item
            {
                Name = "Pass",
                SellIn = -1,
                Quality = 10
            };

            sut.UpdatePassItem();
            
            Assert.AreEqual(0, sut.Quality);
        }

        [Test]
        public void GivenNegativeQuality_WhenUpdatePassItem_ThenQualityIsZero()
        {
            sut = new Item
            {
                Name = "Pass",
                SellIn = 10,
                Quality = -10
            };

            sut.UpdatePassItem();
            
            Assert.AreEqual(0, sut.Quality);
        }

        [Test]
        public void Given55Quality_WhenUpdatePassItem_ThenQualityIs50()
        {
            sut = new Item
            {
                Name = "Pass",
                SellIn = 10,
                Quality = 55
            };

            sut.UpdatePassItem();
            
            Assert.AreEqual(50, sut.Quality);
        }

        [Test]
        public void GivenPositiveSellIn_WhenUpdateConjuredItem_ThenSellInDecreasedByOne()
        {
            var originalSellIn = 20;
            sut = new Item
            {
                Name = "Conjured",
                SellIn = originalSellIn,
                Quality = 20
            };
            
            sut.UpdateConjuredItem();
            
            Assert.AreEqual(originalSellIn - 1, sut.SellIn);
        }

        [Test]
        public void GivenQuality15AndPositiveSellIn_WhenUpdateConjuredItem_ThenQualityIs13()
        {
            var originalQuality = 15;
            sut = new Item
            {
                Name = "Conjured",
                SellIn = 20,
                Quality = originalQuality
            };
            
            sut.UpdateConjuredItem();
            
            Assert.AreEqual(originalQuality - 2, sut.Quality);
        }

        [Test]
        public void GivenQuality15AndZeroSellIn_WhenUpdateConjuredItem_ThenQualityIs11()
        {
            var originalQuality = 15;
            sut = new Item
            {
                Name = "Conjured",
                SellIn = 0,
                Quality = originalQuality
            };
            
            sut.UpdateConjuredItem();
            
            Assert.AreEqual(originalQuality - 4, sut.Quality);
        }

        [Test]
        public void GivenQuality15AndNegativeSellIn_WhenUpdateConjuredItem_ThenQualityIs11()
        {
            var originalQuality = 15;
            sut = new Item
            {
                Name = "Conjured",
                SellIn = -10,
                Quality = originalQuality
            };
            
            sut.UpdateConjuredItem();
            
            Assert.AreEqual(originalQuality - 4, sut.Quality);
        }

        [Test]
        public void GivenQuality55_WhenUpdateConjuredItem_ThenQualityIs50()
        {
            sut = new Item
            {
                Name = "Conjured",
                SellIn = 10,
                Quality = 55
            };
            
            sut.UpdateConjuredItem();
            
            Assert.AreEqual(50, sut.Quality);
        }

        [Test]
        public void GivenQualityNegative_WhenUpdateConjuredItem_ThenQualityIsZero()
        {
            sut = new Item
            {
                Name = "Conjured",
                SellIn = 10,
                Quality = -5
            };
            
            sut.UpdateConjuredItem();
            
            Assert.AreEqual(0, sut.Quality);
        }
    }
}